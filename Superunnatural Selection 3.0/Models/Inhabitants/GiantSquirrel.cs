﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Superunnatural_Selection_3_0
{
    public class GiantSquirrel : Creature, IAge
    {
        public int Age { get; set; }
        public double TailLength { get; set; }

        public GiantSquirrel() : base()
        {

        }

        public GiantSquirrel(int dangerLevel, double tailLength) : base(dangerLevel)
        {
            this.TailLength = tailLength;
        }

        public GiantSquirrel(int hp, string name, char gender, int dangerLevel, double tailLength) : base(hp, name, gender, dangerLevel)
        {
            this.TailLength = tailLength;
        }

        /// <summary>
        /// Ages the inhabitant
        /// </summary>
        public void AgeAYear()
        {
            this.DangerLevel -= 1;
        }

        /// <summary>
        /// Indicates who gets all of this inhabitants belongings
        /// </summary>
        /// <returns>Details about who gets all of this inhabitants belongings</returns>
        public string GetWill()
        {
            return "All my nuts stay in my tree";
        }
    }
}
