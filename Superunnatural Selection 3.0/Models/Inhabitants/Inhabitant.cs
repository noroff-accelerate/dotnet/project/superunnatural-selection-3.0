﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Superunnatural_Selection_3_0
{
    public abstract class Inhabitant : IKillable
    {
        public int Hp { get; set; }
        public string Name { get; set; }
        public bool IsAlive { get; set; }
        public char Gender { get; }

        public Inhabitant()
        {
            this.IsAlive = true;
            this.Hp = 100;
            this.Name = "Nameless";
            this.Gender = '?';
        }

        public Inhabitant(int hp, string name, char gender)
        {
            this.IsAlive = true;
            this.Hp = hp;
            this.Name = name;
            this.Gender = gender;
        }

        public void Die()
        {
            IsAlive = false;
            Hp = 0;
        }
    }
}
