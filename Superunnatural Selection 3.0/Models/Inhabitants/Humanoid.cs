﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Superunnatural_Selection_3_0
{
    public abstract class Humanoid : Inhabitant, ISkilled, IAge
    {
        public int Age { get; set; }
        public List<Skill> Skills { get; set; }

        public Humanoid ():base()
        {
            Skills = new List<Skill>();
        }

        public Humanoid(int hp, string name, char gender):base(hp, name,gender)
        {
            Skills = new List<Skill>();
        }

        public string AcquireSkill(string skillName, string skillDesc, string skillCategory )
        {
            Skill newSkill = new Skill(skillName, skillDesc, skillCategory);
            this.Skills.Add(newSkill);
            return newSkill.Name;
        }

        public string AcquireSkill(Skill newSkill)
        {
            this.Skills.Add(newSkill);
            return newSkill.Name;
        }

        public virtual void SkillSetup()
        {
            Skill basicHumanSkill = new Skill("Stand", "Proficient in standing upright", "Movement");
            Skills.Add(basicHumanSkill);
        }

        public string GetSkillList()
        {
            return Skills.ToString();
        }


        /// <summary>
        /// Indicates who gets all of this inhabitants belongings
        /// </summary>
        /// <returns>Details about who gets all of this inhabitants belongings</returns>
        public string GetWill()
        {
            return "All my stuff goes to Tim";
        }

        /// <summary>
        /// Ages the inhabitant
        /// </summary>
        public void AgeAYear()
        {
            this.Hp -= 1;
        }
    }
}

