﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Superunnatural_Selection_3_0
{
    class PoliticallyCorrectBuddySystem<T,U>
    {
        public T PrimaryBuddy { get; set; }
        public U AltPrimaryBuddy { get; set; }
        public T SecondarySameBuddy { get; set; }
        public U AltSecondaryBuddy { get; set; }


        public PoliticallyCorrectBuddySystem(T primaryBuddy, T secondarySameBuddy)
        {
            this.PrimaryBuddy = primaryBuddy;
            this.SecondarySameBuddy = secondarySameBuddy;
        }

        public PoliticallyCorrectBuddySystem(T primaryBuddy, U altPrimaryBuddy)
        {
            this.PrimaryBuddy = primaryBuddy;
            this.AltPrimaryBuddy = altPrimaryBuddy;
        }

        public PoliticallyCorrectBuddySystem(U altPrimaryBuddy, U altSecondaryBuddy)
        {
            this.AltPrimaryBuddy = altPrimaryBuddy;
            this.AltSecondaryBuddy = altSecondaryBuddy;
        }

        public void HighFive()
        {
            Console.Write("You just witnessed a ");

            if (PrimaryBuddy != null)
            {
                Console.Write($"{PrimaryBuddy.GetType().Name}.....");
            }

            if (AltPrimaryBuddy != null)
            {
                Console.Write($"{AltPrimaryBuddy.GetType().Name}.....");
            }

            if (SecondarySameBuddy != null)
            {
                Console.Write($"{SecondarySameBuddy.GetType().Name}.....");
            }

            if (AltSecondaryBuddy != null)
            {
                Console.Write($"{AltSecondaryBuddy.GetType().Name}......");
            }

            Console.WriteLine(".... high five");
        }
    }
}
