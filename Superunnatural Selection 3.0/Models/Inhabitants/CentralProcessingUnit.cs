﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Superunnatural_Selection_3_0
{
    class CentralProcessingUnit
    {
        public int Cores { get; set; }
        public double Gigahertz { get; set; }

        public CentralProcessingUnit(int cores, double gigahertz)
        {
            this.Cores = cores;
            this.Gigahertz = gigahertz;
        }

        public void Process()
        {
            Random processRnd = new Random();

            for (int i = 0; i < processRnd.Next(150); i++)
            {
                Console.WriteLine("Processing........");
            }
        }
    }
}
