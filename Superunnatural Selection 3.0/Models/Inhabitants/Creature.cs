﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Superunnatural_Selection_3_0
{
    public class Creature :Inhabitant
    {
        public int DangerLevel { get; set; }


        public Creature()
        {
            this.DangerLevel = 50;
        }

        public Creature(int dangerLevel) :base()
        {
            this.DangerLevel = dangerLevel;
        }

        public Creature(int hp, string name, char gender, int dangerLevel):base(hp,name,gender)
        {
            this.DangerLevel = dangerLevel;
        }


    }
}
