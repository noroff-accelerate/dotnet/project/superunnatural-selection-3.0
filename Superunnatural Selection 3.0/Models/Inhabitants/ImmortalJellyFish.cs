﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Superunnatural_Selection_3_0
{
    class ImmortalJellyFish: Creature
    {
        public void Regenerate()
        {
            this.DangerLevel += 2;
        }
    }
}
