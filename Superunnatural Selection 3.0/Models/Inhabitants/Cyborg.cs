﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Superunnatural_Selection_3_0
{
    class Cyborg : Humanoid
    {
        protected CentralProcessingUnit Brain { get; set; }

        //Aggregation - this property may be instantiated (optional i.e. does not occur in any constructor)
        public Skin Skin { get; set; }

        public Cyborg() : base(1000, "", 'n')
        {
            //Composition - always created
            Brain = new CentralProcessingUnit(4, 1.8);
            SkillSetup();
        }

        public Cyborg(int hp, string name, char gender, int cores, double ghz) : base(1000, name, gender)
        {
            //Composition - always created
            Brain = new CentralProcessingUnit(cores, ghz);
            SkillSetup();
        }

        public override void SkillSetup()
        {
            Skill basicHumanSkill = new Skill("Stand", "Proficient in standing upright", "Movement");
            Skill baseSkill = new Skill("Knitting", "Proficient in knitting", "Garmenting");
            Skill baseSkill2 = new Skill("Sprouting", "Proficient in sprouting seeds", "Agriculture");
            Skill baseSkill3 = new Skill("Contortionism", "Proficient in Contortionism", "Movement");

            this.AcquireSkill(basicHumanSkill);
            this.AcquireSkill(baseSkill);
            this.AcquireSkill(baseSkill2);
            this.AcquireSkill(baseSkill3);
        }
    }
}
