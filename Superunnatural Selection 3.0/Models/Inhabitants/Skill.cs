﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Superunnatural_Selection_3_0
{
    public class Skill
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }

        public Skill(string name, string description, string category)
        {
            this.Name = name;
            this.Description = description;
            this.Category = category;
        }
    }
}
