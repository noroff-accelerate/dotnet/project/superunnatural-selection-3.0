﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Superunnatural_Selection_3_0
{
    class Australopithecus : Humanoid
    {

        public Australopithecus():base()
        {
            SkillSetup();
        }

        public Australopithecus(int hp, string name, char gender):base(hp,name,gender)
        {
            SkillSetup();
        }

        /// <summary>
        /// Creates the skill set for Australopithecus
        /// </summary>
        public override void SkillSetup()
        {

            Skill basicHumanSkill = new Skill("Stand", "Proficient in standing upright", "Movement");
            Skill baseSkill = new Skill("Kickboxing", "Proficient in kicking and boxing", "Self Defence");
            Skill baseSkill2 = new Skill("Sprouting", "Proficient in sprouting seeds", "Agriculture");
            
            this.AcquireSkill(basicHumanSkill);
            this.AcquireSkill(baseSkill);
            this.AcquireSkill(baseSkill2);

        }





    }
}
