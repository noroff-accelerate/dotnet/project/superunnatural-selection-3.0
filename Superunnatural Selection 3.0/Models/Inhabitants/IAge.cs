﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Superunnatural_Selection_3_0
{
    public interface IAge
    {
        public int Age { get; set; }
        public void AgeAYear();
        public string GetWill();
    }
}
