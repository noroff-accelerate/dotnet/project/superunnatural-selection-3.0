﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Superunnatural_Selection_3_0
{
    public interface ISkilled
    {
        List<Skill> Skills { get; set; }
        public void SkillSetup();
        public string GetSkillList();
    }
}
