﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Superunnatural_Selection_3_0
{
    class Skin
    {
        public string Type { get; set; }
        public string Color { get; set; }

        public Skin(string type, string color)
        {
            this.Type = type;
            this.Color = color;
        }
    }
}
