﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Superunnatural_Selection_3_0
{
    class BuddySystem<T>
    {

        private T buddy;
        private T otherBuddy;

        public T Buddy { get => buddy; set => buddy = value; }
        public T OtherBuddy { get => otherBuddy; set => otherBuddy = value; }


        public BuddySystem(T buddy, T otherBuddy)
        {

            this.buddy = buddy;
            this.otherBuddy = otherBuddy;

        }

        public void HighFive()
        {
            Console.WriteLine($"You just witnessed a {buddy.GetType().Name}....{otherBuddy.GetType().Name}....high five");

        }
    }
}
