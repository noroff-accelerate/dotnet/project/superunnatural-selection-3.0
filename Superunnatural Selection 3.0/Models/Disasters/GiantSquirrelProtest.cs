﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Superunnatural_Selection_3_0
{
    class GiantSquirrelProtest:UnnaturalDisaster
    {
        private int size;
        List<GiantSquirrel> protestors;
        private Random rnd = new Random();

        public GiantSquirrelProtest(string name, string description, int annualProbabilityLevel, string tagArt,
            int size) : base(name, description, annualProbabilityLevel, tagArt)
        {
            protestors = new List<GiantSquirrel>();
            this.size = size;

        }

        // Polymorphism, each disaster effects the population types differently
        public override void Happen(List<Inhabitant> population)
        {
            var inhabitantSampleName = population[0].GetType().Name;


            int alivePopulationCount = (from inhabitant in population
                where inhabitant.IsAlive == true
                select inhabitant).Count();

            int casualties = 0;
            switch (inhabitantSampleName)
            {
                case "GiantSquirrel":
                    Console.WriteLine($"Giant Squirrels Need Giant Nuts!!!  {this.Name}");
                    break;
                case "ImmortalJellyFish":
                    Console.WriteLine(
                        $"No {inhabitantSampleName} lost their lives, giant squirrels are terrified of jellyfish");
                    break;
                case "Australopithecus":
                    casualties = rnd.Next(20);
                    Damage(population, casualties);
                    Console.WriteLine(
                        $"{casualties} {inhabitantSampleName} lost their lives :(... {inhabitantSampleName} are no match for Giant Squirrels");
                    break;
                case "Cyborg":
                    Console.WriteLine(
                        $"No {inhabitantSampleName} lost their lives, giant squirrels are no match for a {inhabitantSampleName}");
                    break;
            }
        }

    }
}
