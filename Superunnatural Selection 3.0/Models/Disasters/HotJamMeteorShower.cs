﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Superunnatural_Selection_3_0
{
    public class HotJamMeteorShower : UnnaturalDisaster
    {
        private string flavour;
        private Random rnd = new Random();

        public HotJamMeteorShower(string name, string description, int annualProbabilityLevel, string tagArt,
            string flavour) : base(name, description, annualProbabilityLevel, tagArt)
        {
            this.flavour = flavour;
        }

        // Polymorphism, each disaster effects the population types differently
        public override void Happen(List<Inhabitant> population)
        {
            var inhabitantSampleName = population[0].GetType().Name;


            int alivePopulationCount = (from inhabitant in population
                where inhabitant.IsAlive == true
                select inhabitant).Count();

            int casualties = 0;
            int revived = 0;

            switch (inhabitantSampleName)
            {
                case "GiantSquirrel":
                    casualties = rnd.Next(10);
                    Damage(population, casualties);
                    Console.WriteLine(
                        $"{casualties} {inhabitantSampleName} lost their lives :( what a mess!");
                    break;
                case "ImmortalJellyFish":
                    casualties = rnd.Next(40);
                    Damage(population, casualties);
                    Console.WriteLine(
                        $"That was catastrophic for the {inhabitantSampleName}!! hot jam raises the sea temperature! {casualties} {inhabitantSampleName} lost their lives :(");
                    break;
                case "Australopithecus":
                    casualties = rnd.Next(30);
                    Damage(population, casualties);
                    Console.WriteLine(
                        $"{casualties} {inhabitantSampleName} lost their lives :(... The jam is so hot!");
                    revived = rnd.Next(15);
                    Resurrection(population, revived);
                    Console.WriteLine(
                        $"{inhabitantSampleName} now have a years supply of jam for their toast, {revived} {inhabitantSampleName} were saved from certain starvation :)");
                    break;
                case "Cyborg":
                    casualties = rnd.Next(70);
                    Damage(population, casualties);
                    Console.WriteLine(
                        $"That was catastrophic for the {inhabitantSampleName}!! hot jam gets all up in their hydraulics! {casualties} {inhabitantSampleName} were decommissioned:(");
                    break;
            }
        }


    }
}
