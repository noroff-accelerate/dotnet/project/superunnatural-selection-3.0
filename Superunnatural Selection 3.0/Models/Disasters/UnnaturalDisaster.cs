﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Superunnatural_Selection_3_0
{
    public abstract class UnnaturalDisaster
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string TagArt { get; set; }
        public int AnnualProbabilityLevel { get; set; }
        public int Occurences { get; set; }

        protected UnnaturalDisaster(string name, string description, int annualProbabilityLevel, string tagArt)
        {
            this.Name = name;
            this.Description = description;
            this.AnnualProbabilityLevel = annualProbabilityLevel;
            this.TagArt = tagArt;
        }

        /// <summary>
        /// Inflicts loss of life on a population as a result of a disaster
        /// </summary>
        /// <param name="population">The group of inhabitants who will receive the damage</param>
        /// <param name="casualties">The number of inhabitants who lose their life</param>
        public virtual void Damage(List<Inhabitant> population, int casualties)
        {
            IEnumerable<Inhabitant> alivePopulation = from inhabitant in population
                where inhabitant.IsAlive == true
                select inhabitant;

            if (casualties >= alivePopulation.Count() && alivePopulation.Count() != 0)
            {
                foreach (var inhabitant in population)
                {
                    inhabitant.Die();
                }
            }

            else
            {
                int victimCount = 0;

                while (victimCount < casualties && alivePopulation.Count() != 0)
                {
                    foreach (var inhabitant in population)
                    {
                        if (inhabitant.IsAlive != true) continue;
                        inhabitant.IsAlive = false;
                        victimCount++;
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Revives the lives of some inhabitants on a population as a result of a disaster
        /// </summary>
        /// <param name="population">The group of inhabitants who will have some members resurrected</param>
        /// <param name="woke">The number of inhabitants who will be brought back to life</param>
        public virtual void Resurrection(List<Inhabitant> population, int woke)
        {
            IEnumerable<Inhabitant> deadPopulation = from inhabitant in population
                where inhabitant.IsAlive == false
                select inhabitant;

            if (woke >= deadPopulation.Count() && deadPopulation.Count() != 0)
            {
                foreach (var inhabitant in population)
                {
                    inhabitant.IsAlive = true;
                }
            }

            else
            {
                int reviveCount = 0;

                while (reviveCount < woke && deadPopulation.Count() != 0)
                {
                    foreach (var inhabitant in population)
                    {
                        if (inhabitant.IsAlive != false) continue;
                        inhabitant.IsAlive = true;
                        inhabitant.Hp = 50;
                        reviveCount++;
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Effects of the disaster on the group of inhaibants
        /// </summary>
        /// <param name="subPopulation">Group of inhabitants which are exposed to the disaster </param>
        public abstract void Happen(List<Inhabitant> subPopulation);
    }
}
