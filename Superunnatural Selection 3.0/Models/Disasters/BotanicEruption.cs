﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Superunnatural_Selection_3_0
{
    class BotanicEruption :UnnaturalDisaster
    {
        private Random rnd = new Random();
        private string floralGenome;


        public BotanicEruption(string name, string description, int annualProbabilityLevel, string tagArt,
            string floralGenome) : base(name, description, annualProbabilityLevel, tagArt)
        {
            this.floralGenome = floralGenome;
        }

        // Polymorphism, each disaster effects the population types differently
        public override void Happen(List<Inhabitant> population)
        {
            var inhabitantSampleName = population[0].GetType().Name;


            int alivePopulationCount = (from inhabitant in population
                where inhabitant.IsAlive == true
                select inhabitant).Count();

            int casualties = 0;

            switch (inhabitantSampleName)
            {
                case "GiantSquirrel":
                    if (floralGenome == "Daisy")
                    {
                        casualties = rnd.Next(10);
                        Damage(population, casualties);
                        Console.WriteLine(
                            $"{casualties} {inhabitantSampleName} lost their lives :(... They are allergic to Daisies!");
                    }
                    else
                    {
                        Console.WriteLine(
                            $"No {inhabitantSampleName} lost their lives, they have incredibly robust sinuses");
                    }

                    break;

                case "ImmortalJellyFish":
                    Console.WriteLine(
                        $"No {inhabitantSampleName} lost their lives, they do not have sinuses");
                    break;

                case "Australopithecus":
                    casualties = rnd.Next(40);
                    Damage(population, casualties);
                    Console.WriteLine(
                        $"{casualties} {inhabitantSampleName} lost their lives :(... They are allergic to all pollens!");
                    break;
                case "Cyborg":
                    Console.WriteLine(
                        $"No {inhabitantSampleName} lost their lives, they do not have sinuses");
                    break;
            }
        }
    }
}
