﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Superunnatural_Selection_3_0
{
    class KaleStorm : UnnaturalDisaster
    {

        private string style;
        private Random rnd = new Random();

        public KaleStorm(string name, string description, int annualProbabilityLevel, string tagArt, string style) :
            base(name, description, annualProbabilityLevel, tagArt)
        {
            this.style = style;
        }

        // Polymorphism, each disaster effects the population types differently
        public override void Happen(List<Inhabitant> population)
        {
            var inhabitantSampleName = population[0].GetType().Name;


            int alivePopulationCount = (from inhabitant in population
                where inhabitant.IsAlive == true
                select inhabitant).Count();

            int revived = 0;

            switch (inhabitantSampleName)
            {
                case "GiantSquirrel":
                    revived = rnd.Next(10);
                    Resurrection(population, revived);
                    Console.WriteLine(
                        $"{revived} {inhabitantSampleName} were resurrected :) {inhabitantSampleName} live for kale");
                    break;
                case "ImmortalJellyFish":
                    Console.WriteLine(
                        $"Kale means nothing to {inhabitantSampleName}");
                    break;
                case "Australopithecus":
                    revived = rnd.Next(10);
                    Resurrection(population, revived);
                    Console.WriteLine(
                        $"{revived} {inhabitantSampleName} were resurrected :) {inhabitantSampleName} live for kale");
                    break;
                case "Cyborg":
                    Console.WriteLine(
                        $"A {inhabitantSampleName} is indifferent to Kale");
                    break;
            }
        }
    }
}
