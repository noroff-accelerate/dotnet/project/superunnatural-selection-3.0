﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Superunnatural_Selection_3_0.Models.Environment
{
    public abstract class Foliage : IAge

    {
        public int Age { get; set; }
        public string Colour { get; set; }
        public string Genome { get; set; }

        public virtual void AgeAYear()
        {
            Console.WriteLine("This will take awhile");
        }

        public virtual string GetWill()
        {
            return "I have nothing";
        }
    }
}
