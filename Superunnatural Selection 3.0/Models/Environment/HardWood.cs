﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Superunnatural_Selection_3_0.Models.Environment
{
    abstract class HardWood : Foliage
    {
        public double GrainCount { get; set; }

        public double Height { get; set; }
    }
}
