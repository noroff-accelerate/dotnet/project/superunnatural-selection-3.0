﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Superunnatural_Selection_3_0.Models.Environment
{
    class RoseWoodTree : HardWood
    {
        public string Shade { get; set; }
    }
}
