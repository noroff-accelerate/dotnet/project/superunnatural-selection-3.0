﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using Superunnatural_Selection_3_0.Models.Environment;

namespace Superunnatural_Selection_3_0
{
    class Program
    {
        static void Main(string[] args)
        {
            Cyborg test = new Cyborg
            {
            };

            ShowHeader();

            #region Create Objects and Collections

            PopulationFilterer populationFilterer = new PopulationFilterer();
            Dictionary<string, List<Inhabitant>> population = new Dictionary<string, List<Inhabitant>>();
            List<UnnaturalDisaster> climate = new List<UnnaturalDisaster>();
            Dictionary<string, List<Foliage>> environment = new Dictionary<string, List<Foliage>>();
            List<Inhabitant> australopithecusPopulation = new List<Inhabitant>();
            List<Inhabitant> cyborgPopulation = new List<Inhabitant>();
            List<Inhabitant> giantSquirrelPopulation = new List<Inhabitant>();
            List<Inhabitant> immortalJellyFishPopulation = new List<Inhabitant>();
            List<Foliage> lichenFoliage = new List<Foliage>();
            List<Foliage> roseWoodFoliage = new List<Foliage>();
            Random rnd = new Random();

            // Instantiate and store Unnatural Disasters
            HotJamMeteorShower hotJamMeteorShower =
                new HotJamMeteorShower("Koo", "Unpleasant shower of molten jam", rnd.Next(10, 51),
                    " / / / / / / / / / / ", "Blueberry");
            BotanicEruption botanicEruption = new BotanicEruption("Daisy",
                "Unpleasant world wide wild sprouting and flowering of Daisies", rnd.Next(10, 51),
                " @}}>----- @}}>----- @}}>-----", "Daisy");
            KaleStorm kaleStorm = new KaleStorm("Mary", "It is raining fresh crispy Kale", rnd.Next(10, 51),
                " * * * * * * * * * * *",
                "Raw and Crispy");
            GiantSquirrelProtest giantSquirrelProtest = new GiantSquirrelProtest("#NoMoreNutGrabbing",
                "A volatile protest against nut butter manufacturers", rnd.Next(10, 51), "(-_ - ;)  (-_ - ;)  (-_ - ;)",
                rnd.Next(51));

            climate.Add(hotJamMeteorShower);
            climate.Add(botanicEruption);
            climate.Add(kaleStorm);
            climate.Add(giantSquirrelProtest);

            // Generate and store Foliage
            for (int i = 0; i < rnd.Next(1000, 5000); i++)
            {
                lichenFoliage.Add(new Lichen
                {
                    Age = rnd.Next(200),
                    Colour = "Probably greenish green",
                    Genome = "Unknown",
                    RockTypePreference = "Grey and smooth",
                });
            }

            environment.Add("Lichen", lichenFoliage);

            for (int i = 0; i < rnd.Next(1000, 5000); i++)
            {
                roseWoodFoliage.Add(new RoseWoodTree
                {
                    Age = rnd.Next(900),
                    Colour = "Probably greenish green",
                    Genome = "Unknown",
                    GrainCount = rnd.Next(4000),
                    Height = rnd.Next(50),
                    Shade = "Pinkish Redish Pinky Red"
                });
            }

            environment.Add("Rose Wood", roseWoodFoliage);

            // Generate inhabitant objects to populate sub populations for the age simulator
            int populationSize = rnd.Next(50, 200);
            for (int i = 0; i < populationSize; i++)
            {
                giantSquirrelPopulation.Add(new GiantSquirrel());
            }

            population.Add("Giant Squirrel Population", giantSquirrelPopulation);

            populationSize = rnd.Next(50, 200);
            for (int i = 0; i < populationSize; i++)
            {
                immortalJellyFishPopulation.Add(new ImmortalJellyFish());
            }

            population.Add("Immortal Jellyfish Population", immortalJellyFishPopulation);

            populationSize = rnd.Next(50, 200);
            for (int i = 0; i < populationSize; i++)
            {
                australopithecusPopulation.Add(new Australopithecus());
            }

            population.Add("Australopithecus Population", australopithecusPopulation);

            populationSize = rnd.Next(50, 200);
            for (int i = 0; i < populationSize; i++)
            {
                cyborgPopulation.Add(new Cyborg());
            }

            population.Add("Cyborg Population", cyborgPopulation);

            // Create SuperunnaturalAgeSimulator simulator object
            SuperunnaturalAgeSimulator age = new SuperunnaturalAgeSimulator();

            #endregion

            #region Execute Object behaviours and interaction

            //Load SuperunnaturalAgeSimulator with the population and environment created above
            age.Load(population, climate, environment);
            // Display logic
            Console.SetWindowSize(Console.WindowWidth, 40);
            Console.WriteLine("\n<')++< PRESS THE ENTER KEY TO BEGIN THE SIMULATION >++('>");
            Console.ReadLine();

            // Begin simulation
            age.Simulate(10);

            #endregion
        }

        public static void ShowHeader()
        {
            string title = @"

 (                                                                       (                                    
 )\ )                                                )              (    )\ )     (            )              
(()/(  (          (  (        (                 ) ( /(  (  (      ) )\  (()/(   ( )\  (     ( /((             
 /(_))))\ `  )   ))\ )(       )\  (     (    ( /( )\())))\ )(  ( /(((_)  /(_)) ))((_)))\ (  )\())\  (   (     
(_)) /((_)/(/(  /((_|()\   _ ((_) )\ )  )\ ) )(_)|_))//((_|()\ )(_))_   (_))  /((_) /((_))\(_))((_) )\  )\ )  
/ __(_))(((_)_\(_))  ((_) | | | |_(_/( _(_/(((_)_| |_(_))( ((_|(_)_| |  / __|(_))| (_)) ((_) |_ (_)((_)_(_/(  
\__ \ || | '_ \) -_)| '_| | |_| | ' \)) ' \)) _` |  _| || | '_/ _` | |  \__ \/ -_) / -_) _||  _|| / _ \ ' \)) 
|___/\_,_| .__/\___||_|    \___/|_||_||_||_|\__,_|\__|\_,_|_| \__,_|_|  |___/\___|_\___\__| \__||_\___/_||_|  
         |_|
 __  (\_         __  (\_         __  (\_         __  (\_         __  (\_         __  (\_         __  (\_        __  (\_ 
(_ \ ( '>       (_ \ ( '>       (_ \ ( '>       (_ \ ( '>       (_ \ ( '>       (_ \ ( '>       (_ \ ( '>      (_ \ ( '>
  ) \/_)=         ) \/_)=         ) \/_)=         ) \/_)=         ) \/_)=         ) \/_)=         ) \/_)=        ) \/_)=
  (_(_ )_         (_(_ )_         (_(_ )_         (_(_ )_         (_(_ )_         (_(_ )_         (_(_ )_        (_(_ )_
";
            Console.WriteLine(title);
        }
    }
}
