﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Linq;

namespace Superunnatural_Selection_3_0
{
    class PopulationFilterer
    {
        
        //For demonstrating LINQ
        //Will include IDisposable for this type of custom class in future - very applicable to classes which provide services as they can be created used and disposed easily on the fly 
        public PopulationFilterer()
        {

        }

        public List<Skill> GetCollectiveUniqueAgricultureSkillSet(List<Humanoid> humanoids)
        {
            if (CheckHomogeniousness(humanoids))
            {
                IEnumerable<Skill> skillSet =
                        from skill in humanoids[0].Skills
                        where skill.Category == "Agriculture"
                        select skill;

                return skillSet.ToList();
            }

            return new List<Skill>();

        }

        public bool CheckHomogeniousness(List<Humanoid> humanoids)
        {
            var currentType = humanoids[0].GetType();

            foreach (Humanoid individual in humanoids)
            {

                if(individual.GetType() != currentType)
                {

                    return false;
                }

            }

            return true;
        }

       










    }
}
