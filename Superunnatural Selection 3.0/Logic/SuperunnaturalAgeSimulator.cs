﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Channels;
using Superunnatural_Selection_3_0.Models.Environment;

namespace Superunnatural_Selection_3_0
{
    public class SuperunnaturalAgeSimulator
    {
        private Dictionary<string, List<Inhabitant>> population;
        private List<UnnaturalDisaster> climate;
        private Dictionary<string, List<Foliage>> environment;
        private Random probabilityEngine = new Random();


        public SuperunnaturalAgeSimulator()
        {
            population = new Dictionary<string, List<Inhabitant>>();
            climate = new List<UnnaturalDisaster>();
            environment = new Dictionary<string, List<Foliage>>();
        }

        public void Load(Dictionary<string, List<Inhabitant>> population, List<UnnaturalDisaster> climate,
            Dictionary<string, List<Foliage>> environment)
        {
            Console.WriteLine($"AGE LOADED! Ready to begin simulation");

            this.population = population;
            this.climate = climate;
            this.environment = environment;

            Console.WriteLine("-------------------------------------------");
            Console.WriteLine("The Population contains sub-populations of: ");
            Console.WriteLine("-------------------------------------------");
            foreach (var subPopulation in this.population)
            {
                var inhabitantSample = subPopulation.Value[0];
                Console.WriteLine($"{inhabitantSample.GetType().Name}, population: {subPopulation.Value.Count}");
            }

            Console.WriteLine();
            Console.WriteLine("--------------------------------------------------------------");
            Console.WriteLine("The following unnatural disasters could happen in this climate");
            Console.WriteLine("--------------------------------------------------------------");
            foreach (var unnaturalDisaster in this.climate)
            {
                Console.WriteLine(
                    $"{unnaturalDisaster.GetType().Name}, annual probability: {unnaturalDisaster.AnnualProbabilityLevel}%");
            }

            Console.WriteLine();
            Console.WriteLine("------------------------------------------------");
            Console.WriteLine("The following foliage exists in this environment");
            Console.WriteLine("------------------------------------------------");
            foreach (var foliage in this.environment)
            {
                var foliageSample = foliage.Value[0];
                Console.WriteLine(
                    $"{foliageSample.GetType().Name}, total: {foliage.Value.Count}");
            }
        }

        public void Simulate(int years)
        {
            ProbabilityCalibration();

            for (int i = 0; i < years; i++)
            {
                Console.Clear();
                ShowHeader();
                Console.WriteLine($"HAPPY NEW YEAR!!! it is year {i + 1}\n");
                Console.WriteLine($"SIMULATING 1 YEAR");
                for (int j = 0; j < 9; j++)
                {
                    Thread.Sleep(150);
                    Console.Write(".");
                }

                Console.WriteLine("\n-------------------------------------------------------\n");
                foreach (var disaster in climate)
                {
                    if (probabilityEngine.Next(0, 100) < disaster.AnnualProbabilityLevel)
                    {
                        Console.WriteLine($"{disaster.TagArt}");
                        Console.WriteLine("!!DISASTER!!");
                        Console.Write($"A {disaster.GetType().Name} NAMED {disaster.Name} HAPPENED: ");
                        disaster.Occurences++;

                        foreach (var subPopulation in this.population)
                        {
                            disaster.Happen(subPopulation.Value);
                        }

                        Console.WriteLine($"{disaster.TagArt}\n");
                    }
                    else
                    {
                        Console.WriteLine($"{disaster.TagArt}");
                        Console.WriteLine($"NO {disaster.GetType().Name} THIS YEAR :)");
                        Console.WriteLine($"{disaster.TagArt}\n");
                    }
                }

                Console.WriteLine("-------------------------------------------------------");
                Console.WriteLine("\n¤¤¤ POPULATION STATUS ¤¤¤\n");
                foreach (var subPopulation in population)
                {
                    var inhabitantSample = subPopulation.Value[0];
                    int alivePopulationCount = (from inhabitant in subPopulation.Value
                        where inhabitant.IsAlive == true
                        select inhabitant).Count();
                    Console.WriteLine($"{inhabitantSample.GetType().Name} population: \t\t{alivePopulationCount}");

                    if (inhabitantSample is IAge)
                    {
                        foreach (IAge inhabitant in subPopulation.Value)
                        {
                            inhabitant.AgeAYear();
                        }
                    }
                }

                Console.WriteLine("-------------------------------------------------------");
                Console.WriteLine("PRESS ANY KEY TO ADVANCE A YEAR");
                Console.ReadLine();
            }

            FinalResult();
        }

        public void ProbabilityCalibration()
        {
            // Inspect population and change disaster probabilities
        }

        public void FinalResult()
        {
            using (StreamWriter sw = new StreamWriter("Simulation Report.txt"))
            {
                string resultHeader = @"

 (                                                                       (                                    
 )\ )                                                )              (    )\ )     (            )              
(()/(  (          (  (        (                 ) ( /(  (  (      ) )\  (()/(   ( )\  (     ( /((             
 /(_))))\ `  )   ))\ )(       )\  (     (    ( /( )\())))\ )(  ( /(((_)  /(_)) ))((_)))\ (  )\())\  (   (     
(_)) /((_)/(/(  /((_|()\   _ ((_) )\ )  )\ ) )(_)|_))//((_|()\ )(_))_   (_))  /((_) /((_))\(_))((_) )\  )\ )  
/ __(_))(((_)_\(_))  ((_) | | | |_(_/( _(_/(((_)_| |_(_))( ((_|(_)_| |  / __|(_))| (_)) ((_) |_ (_)((_)_(_/(  
\__ \ || | '_ \) -_)| '_| | |_| | ' \)) ' \)) _` |  _| || | '_/ _` | |  \__ \/ -_) / -_) _||  _|| / _ \ ' \)) 
|___/\_,_| .__/\___||_|    \___/|_||_||_||_|\__,_|\__|\_,_|_| \__,_|_|  |___/\___|_\___\__| \__||_\___/_||_|  
         |_|
 __  (\_         __  (\_         __  (\_         __  (\_         __  (\_         __  (\_         __  (\_        __  (\_ 
(_ \ ( '>       (_ \ ( '>       (_ \ ( '>       (_ \ ( '>       (_ \ ( '>       (_ \ ( '>       (_ \ ( '>      (_ \ ( '>
  ) \/_)=         ) \/_)=         ) \/_)=         ) \/_)=         ) \/_)=         ) \/_)=         ) \/_)=        ) \/_)=
  (_(_ )_         (_(_ )_         (_(_ )_         (_(_ )_         (_(_ )_         (_(_ )_         (_(_ )_        (_(_ )_

AGE SIMULATION RESULTS:
----------------------------------------------------------------------------------------------------------------------------
";
                sw.WriteLine(resultHeader);
                Console.Clear();
                ShowHeader();
                Console.WriteLine("AGE SIMULATION RESULTS:");
                Console.WriteLine(
                    "------------------------------------------------------------------------------------------------------------------------");
                Console.WriteLine("\nPopulation:....");
                sw.WriteLine("\nPopulation:....");
                foreach (var subPopulation in population)
                {
                    var inhabitantSample = subPopulation.Value[0];
                    int alivePopulationCount = (from inhabitant in subPopulation.Value
                        where inhabitant.IsAlive == true
                        select inhabitant).Count();

                    if (alivePopulationCount > 0)
                    {
                        Console.WriteLine($"{inhabitantSample.GetType().Name} population: {alivePopulationCount}");
                        sw.WriteLine($"{inhabitantSample.GetType().Name} population: {alivePopulationCount}");
                    }
                    else
                    {
                        Console.WriteLine($"{inhabitantSample.GetType().Name} went extinct");
                        sw.WriteLine($"{inhabitantSample.GetType().Name} went extinct");
                    }
                }

                Console.WriteLine("\nDisasters:....");
                sw.WriteLine("\nDisasters:....");

                foreach (var disaster in climate)
                {
                    Console.WriteLine($"{disaster.GetType().Name}: Occurred: {disaster.Occurences} times");
                    sw.WriteLine($"{disaster.GetType().Name}: Occurred: {disaster.Occurences} times");
                }
            }


            Console.WriteLine("\nText file of report can be found in bin\\Debug\netcoreapp3.0\\Simulation Report.txt");
        }

        public void ShowHeader()
        {
            string title = @"

 (                                                                       (                                    
 )\ )                                                )              (    )\ )     (            )              
(()/(  (          (  (        (                 ) ( /(  (  (      ) )\  (()/(   ( )\  (     ( /((             
 /(_))))\ `  )   ))\ )(       )\  (     (    ( /( )\())))\ )(  ( /(((_)  /(_)) ))((_)))\ (  )\())\  (   (     
(_)) /((_)/(/(  /((_|()\   _ ((_) )\ )  )\ ) )(_)|_))//((_|()\ )(_))_   (_))  /((_) /((_))\(_))((_) )\  )\ )  
/ __(_))(((_)_\(_))  ((_) | | | |_(_/( _(_/(((_)_| |_(_))( ((_|(_)_| |  / __|(_))| (_)) ((_) |_ (_)((_)_(_/(  
\__ \ || | '_ \) -_)| '_| | |_| | ' \)) ' \)) _` |  _| || | '_/ _` | |  \__ \/ -_) / -_) _||  _|| / _ \ ' \)) 
|___/\_,_| .__/\___||_|    \___/|_||_||_||_|\__,_|\__|\_,_|_| \__,_|_|  |___/\___|_\___\__| \__||_\___/_||_|  
         |_|
 __  (\_         __  (\_         __  (\_         __  (\_         __  (\_         __  (\_         __  (\_        __  (\_ 
(_ \ ( '>       (_ \ ( '>       (_ \ ( '>       (_ \ ( '>       (_ \ ( '>       (_ \ ( '>       (_ \ ( '>      (_ \ ( '>
  ) \/_)=         ) \/_)=         ) \/_)=         ) \/_)=         ) \/_)=         ) \/_)=         ) \/_)=        ) \/_)=
  (_(_ )_         (_(_ )_         (_(_ )_         (_(_ )_         (_(_ )_         (_(_ )_         (_(_ )_        (_(_ )_
";
            Console.WriteLine(title);
        }
    }
}