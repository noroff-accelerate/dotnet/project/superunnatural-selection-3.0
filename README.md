# Superunnatural Selection 3.0

.NET Core Console Application

Intended for learning Foundational OOP as part of module 1 of Noroff Accelerate .NET Fullstack short course.

Demonstrates the use of core OOP concepts


## Getting Started

Clone to a local directory.

Open solution in Visual Studio

Run

### Prerequisites

.NET Framework

Visual Studio 2017/19 OR Visual Studio Code


## Authors

***Dean von Schoultz** [deanvons](https://gitlab.com/deanvons)




